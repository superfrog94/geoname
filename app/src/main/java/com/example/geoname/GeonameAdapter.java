package com.example.geoname;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.List;

public class GeonameAdapter extends RecyclerView.Adapter<GeonameAdapter.ViewHolder> {
    private Context context;
    private List<Geoname> dataGeonamesList;

    public GeonameAdapter(Context context, List<Geoname> dataGeonamesList) {
        this.context = context;
        this.dataGeonamesList = dataGeonamesList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_data, parent, false);
        return new ViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Geoname dataGeonames = dataGeonamesList.get(position);
        holder.lng.setText(dataGeonames.getLng().toString());
        holder.geonameId.setText(dataGeonames.getGeonameId().toString());
        holder.countrycode.setText(dataGeonames.getCountrycode());
        holder.name.setText(dataGeonames.getName());
        holder.fclName.setText(dataGeonames.getFclName());
        holder.toponymName.setText(dataGeonames.getToponymName());
        holder.fcodeName.setText(dataGeonames.getFcodeName());
        holder.wikipedia.setText(dataGeonames.getWikipedia());
        holder.lat.setText(dataGeonames.getLat().toString());
        holder.fcl.setText(dataGeonames.getFcl());
        holder.population.setText(dataGeonames.getPopulation().toString());
        holder.fcode.setText(dataGeonames.getFcode());

    }

    @Override
    public int getItemCount() {
        return dataGeonamesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView cardData;
        TextView lng, geonameId, countrycode, name, fclName,
                toponymName, fcodeName, wikipedia, lat,
                fcl, population, fcode;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardData = itemView.findViewById(R.id.cardViewId);
            lng = itemView.findViewById(R.id.itemLng);
            geonameId = itemView.findViewById(R.id.itemGeonamesID);
            countrycode = itemView.findViewById(R.id.itemCountryCode);
            name = itemView.findViewById(R.id.itemName);
            fclName = itemView.findViewById(R.id.itemFCLName);
            toponymName = itemView.findViewById(R.id.itemToponymName);
            fcodeName = itemView.findViewById(R.id.itemFcodeName);
            wikipedia = itemView.findViewById(R.id.itemWikipedia);
            lat = itemView.findViewById(R.id.itemLat);
            fcl = itemView.findViewById(R.id.itemFcl);
            population = itemView.findViewById(R.id.itemPopulation);
            fcode = itemView.findViewById(R.id.itemFcode);

        }
    }
}
