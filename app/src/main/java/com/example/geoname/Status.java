package com.example.geoname;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Status {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("value")
    @Expose
    private Integer value;

    public void setMessage(String message) {
        this.message = message;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
