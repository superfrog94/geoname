package com.example.geoname;

import retrofit2.Call;
import retrofit2.http.GET;

import retrofit2.http.Url;

public interface GetDataService {

    @GET
    Call<DataModel> getData(@Url String url);
}
