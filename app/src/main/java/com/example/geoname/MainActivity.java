package com.example.geoname;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private String valueNorth, valueSouth, valueEast,
            valueWest, valueLang, valueUser, valueError;
    private GetDataService service;
    DataModel dataModel;
    List<Geoname> dataGeonamesList;
    ProgressDialog progressDialog;
    private RecyclerView itemGeonames;
    private GeonameAdapter geoAdapter;
    private TextView error;
    private EditText inputNorth, inputSouth, inputEast,
            inputWest, inputLang, inputUser;

    private Button btnSubmit;
    private Map<String, String> path;

    DataStatusWeather dataStatus;

    private String url = "http://api.geonames.org/citiesJSON?north=44.1&south=-9.9&east=-22.4&west=55.2&lang=de&username=demos";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupView();
    }

    private void setupView() {
        itemGeonames = findViewById(R.id.itemGeonames);
        error = findViewById(R.id.errorMsg);

        inputNorth = findViewById(R.id.inputNorth);
        inputSouth = findViewById(R.id.inputSouth);
        inputWest = findViewById(R.id.inputWest);
        inputEast = findViewById(R.id.inputEast);
        inputLang = findViewById(R.id.inputLang);
        inputUser = findViewById(R.id.inputUser);

        btnSubmit = findViewById(R.id.submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                valueNorth = "44.1";
                valueSouth = "-9.9";
                valueEast = "-22.4";
                valueWest = "55.2";
                valueLang = "de";
                valueUser = "demos";
                if (!valueNorth.equals(inputNorth.getText().toString()) &&
                        !valueSouth.equals(inputSouth.getText().toString()) &&
                        !valueEast.equals(inputEast.getText().toString()) &&
                        !valueWest.equals(inputWest.getText().toString()) &&
                        !valueLang.equals(inputLang.getText().toString()) &&
                        !valueUser.equals(inputUser.getText().toString())) {
                    Toast.makeText(MainActivity.this, "Connection Problem", Toast.LENGTH_SHORT).show();
                } else {
                    networkRequest();
                }
            }
        });
    }


    private void networkRequest() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

//        valueNorth = inputNorth.getText().toString();
//        valueEast  =  inputEast.getText().toString();
//        valueSouth = inputSouth.getText().toString();
//        valueWest  = inputWest.getText().toString();
//        valueUser  = inputUser.getText().toString();
//        valueLang  = inputLang.getText().toString();

        service = RetrofitClient.getRetrofitInstance().create(GetDataService.class);
        service.getData(url).enqueue(new Callback<DataModel>() {
            @Override
            public void onResponse(Call<DataModel> call, Response<DataModel> response) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Get", Toast.LENGTH_SHORT).show();
                DataModel geoname = response.body();
                if (geoname.getGeonames() != null) {
                    dataGeonamesList = geoname.getGeonames();
                    showRecView(dataGeonamesList);
                } else {
                    String message = dataStatus.getStatus().toString();
                    error.setText(message);
                }
            }

            @Override
            public void onFailure(Call<DataModel> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "{\"status\":{\"message\":\"the daily limit of 20000 credits for demo has been exceeded. Please use an application specific account. Do not use the demo account for your application.\",\"value\":18}}", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void showRecView(List<Geoname> dataGeonamesList) {
        itemGeonames.setLayoutManager(new LinearLayoutManager(this,
                RecyclerView.VERTICAL, false));
        geoAdapter = new GeonameAdapter(this, dataGeonamesList);
        itemGeonames.setAdapter(geoAdapter);


    }


}
